# 1. Complete Search / Brute Force/ Recursive Backtracking
# ex. try all possibilities - print all numbers from 0 to 1000000 that is
# divisible by 7 and contains a digit 3 and a digit 8

# 2. Divide and Conquer
# ex. binary search

# 3. Greedy
# ex. solving a problem using "greedy" choices

# 4. Dynamic Programming
# ex. recursive backtracking + cache

# Iterative Complete Search
# UVa 725 - Division
# Find all 5 digit numbers that use digits 0 to 9 once each such that
# the first number divided by the second is equal to N
# abcde/fghij = N where each letter is a different digit
# the first digit is allowed to be 0
# N = 62, 79546 / 01283 = 62; 94736 / 01528 = 62

N = 62
for fghij in range(1234, 98765 // N + 1):
    abcde = fghij * N
    s = set()
    if fghij < 10000:
        s.add(0)
    tmp = abcde
    while tmp > 0:
        last_digit = tmp % 10
        s.add(last_digit)
        tmp //= 10
    tmp = fghij
    while tmp > 0:
        last_digit = tmp % 10
        s.add(last_digit)
        tmp //= 10
    if len(s) == 10:
        print("{} / {} = {}".format(abcde,fghij,N))

# UVa 441 - Lotto
# 6 <= k < 13 sorted integers: enumerate all possible subsets of size 6 in sorted order
# 1 2 3 4 5 6 7
# 1 2 3 4 5 6, 1 2 3 4 5 7, 1 2 3 4 6 7, ...
# 6 nested for loops

# Simplified variant:
# print out all subsets of size 3 from 1 2 3 4 5 in sorted order
# 1 2 3, 1 2 4, 1 2 5, 1 3 4, 1 3 5, 1 4 5, 2 3 4, 2 3 5, 3 4 5
for i in range(1,4):
    for j in range(i+1,5):
        for k in range(j+1,6):
            print("{} {} {}".format(i,j,k))

# UVa 11565 - Simple Equations
# A, B, C <= 10000: find x, y, z such that x+y+z=A, x*y*z=B, x^2+y^2+z^2=C
# 10000**3=10^12 (too slow)

# UVa 11742 - Social Constraints
# n <= 8 movie goers, n consecutive open seats 
# m <= 20 constaints: person a and person b must be at least (at most) c seats apart
# how many possible seating arrangments are there?
# Tricky thing: how to loop through all permutations
# look into itertools library
# Extra challenge***(hard): implement yourself

