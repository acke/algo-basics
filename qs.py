def qs(arr):
    if len(arr) <= 1:
        return arr
    p = arr[0]
    return qs([x for x in arr if x < p]) + [p] + qs([x for x in arr if x > p])

a = [1,6,4,3,2,7,8,5]
print(a)
print(qs(a))
