row = [0]*8
poss = 0
# row = {1,3,5,7,2,0,6,4}
# row[0] = 1: queen in column 0 is in row 1

def place(r, c):
    global row
    for prev in range(c):
        if row[prev] == r or (abs(r-row[prev]) == abs(c-prev)):
            return False
    return True

def backtrack(c):
    global poss, row
    if c == 8:
        poss += 1
        return
    for r in range(8):
        if place(r,c):
            row[c] = r
            backtrack(c+1)

def main():
    backtrack(0)
    print(poss)

main()

