# UVa 11902 - Dominator
# UVa 459 - Connected Components
# UVa 469 - Wetlands of Florida

grid = [
    [0,0,0,0,0,1],
    [0,1,1,1,0,1],
    [1,1,0,1,0,0],
    [0,1,1,1,1,0],
    [0,0,0,1,1,0],
    [0,1,0,0,1,0]
]

num_rows = len(grid)
num_cols = len(grid[0])

dr = [-1,-1,-1, 0,0, 1,1,1]
dc = [-1, 0, 1,-1,1,-1,0,1]

def print_grid():
    for row in grid:
        print(' '.join([str(i) for i in row]))

def floodfill(r, c, c1, c2):
    if r < 0 or r >= num_rows or c < 0 or c >= num_cols:
        return 0
    if grid[r][c] != c1:
        return 0
    grid[r][c] = c2
    ans = 1
    for i in range(8):
        ans += floodfill(r + dr[i], c + dc[i], c1, c2)
    return ans

print_grid()
r = 1
c = 1
print(floodfill(r,c,grid[r][c],2))
print_grid()

