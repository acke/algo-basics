# Binary Search (built in function)
# use import bisect and can find elements fast

# Binary search answers for problems that seemingly can
# be solved with pure math

"""
You buy a car with a loan and now, you pay the loan back in monthly
installments of d dollars for m months. Suppose the car is originally
v dollars and bank charges interest rate of i% for any unpaid loan at
the end of each month. What is the amount of money d that you must pay
per month?

d = 576.19
m = 2
v = 1000
i = 10%

After 1 month:
debt = 1000*1.1 - 576.19 = 523.81

After 2 months:
debt = 523.81*1.1 - 576.19 ~= 0

Find d such that you get ~= 0

IDEA: Use bisection
"""

def sim(d,m,v,i):
    total_debt = v
    for _ in range(m):
        total_debt = total_debt*(1+i) - d
    return total_debt

m = 2
v = 1000
i = 0.1
low = 0.01
high = v*(1+i)
mid = 0

while abs(high-low) > 1e-3:
    mid = (low+high) / 2
    print(low, high, mid)
    if sim(mid,m,v,i) < 0:
        high = mid
    else:
        low = mid

for n in range(1,110000,1):
    print(n/100, sim(n/100,m,v,i))

"""
UVa 11935: Through the Desert
You are an explorer trying to cross a desert. You use a jeep
with a large enough fuel tank - initially full. Throughout your
journey, there are random events that affect your gas i.e. gas leaks,
gas refill stations, mechanic that fixes leaks, reach goal. You need
to determine smallest possible initial fuel tank size to get to goal.
"""


