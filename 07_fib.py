from functools import lru_cache
import time

@lru_cache(maxsize=None)
def naive_fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return naive_fib(n-1) + naive_fib(n-2)

# Use memoization/caching to speed up
cache = {}

def rec_fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    elif n in cache:
        return cache[n]
    else:
        ans = rec_fib(n-1) + rec_fib(n-2)
        cache[n] = ans
        return ans

s = time.time()
naive_fib(36) # ~8 seconds
f = time.time()
print("Time: {} s".format(f-s))
