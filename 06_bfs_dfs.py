# Unweighted graphs

from queue import Queue

# 5 nodes - each is labeled from 0 to 4
# 0 to 1
# 0 to 2
# 1 to 4
# 2 to 3

adj_list = [
    [1,2],
    [0,4],
    [0,3],
    [2],
    [1]
]

# BFS - breadth first search
def bfs():
    INF = 100000000
    dist = [INF] * 5
    dist[0] = 0

    q = Queue()
    q.put(0)

    while not q.empty():
        u = q.get()
        print("Visited ", u)
        for v in adj_list[u]:
            if dist[v] == INF:
                dist[v] = dist[u] + 1
                q.put(v)

# DFS - depth first search
def dfs_q():
    INF = 100000000
    dist = [INF] * 5
    dist[0] = 0

    s = []
    s.append(0)

    while s:
        u = s.pop()
        print("Visited ", u)
        for v in adj_list[u]:
            if dist[v] == INF:
                dist[v] = dist[u] + 1
                s.append(v)

dfs_num = [False] * 5

def dfs(u):
    dfs_num[u] = True
    print("Visited ", u)
    for v in adj_list[u]:
        if not dfs_num[v]:
            dfs(v)


bfs()
print()
dfs(0)

