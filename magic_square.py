def pretty_print(square):
    for row in square:
        print(' '.join([str(i) for i in row]))

N = 3
square = []
seen = [False] * (N*N+1)
for _ in range(N):
    square.append([0] * N)

def is_okay(r, c, i):
    global square, seen
    if seen[i]:
        return False
    if c == N - 1:
        if sum(square[r]) != 15:
            return False
    if r == N - 1:
        total = 0
        for i in range(N):
            total += square[i][c]
        if total != 15:
            return False
    return True

def backtrack(r, c):
    global square, seen
    pretty_print(square)
    print()
    if r == N:
        pretty_print(square)
        return
    for i in range(1,N*N + 1):
        if is_okay(r, c, i):
            square[r][c] = i
            seen[i] = True
        else:
            continue
        if c == N - 1:
            backtrack(r+1, 0)
        else:
            backtrack(r, c+1)
        seen[i] = False
        square[r][c] = 0
    
backtrack(0,0)

