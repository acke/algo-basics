# Given a number, output two numbers that sum to it that doesn't use 4

# The number can be as big as 10^100

# Simplest idea: given number N, try N-1, 1, then N-2, 2, and so on
# Check if there exists a 4, if so, try the next one; otherwise output
# time takes: size of N * number of digits in N = N * log N * T
# N = 10^9 * 10 > 10^10

# 10^100
N = "2365943268532947623256364"
#    0000030000000030000000003
#    2365913265832917623256361


