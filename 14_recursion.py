# Permutations
# perm(0): []
# perm(1): [(1)]
# perm(2): [(1,2), (2,1)]
# perm(3): [(1,2,3), (1,3,2), (2,1,3), (2,3,1), (3,1,2), (3,2,1)]

def perm(n):
    if n == 0:
        return []
    elif n == 1:
        return [(1,)]
    else:
        prev_perm = perm(n-1)
        ans = []
        for p in prev_perm:
            for i in range(n):
                ans.append(p[:i] + (n,) + p[i:])
        return ans


from string import ascii_letters

# a, b, c
# aa, ab, ac, ba, bb, bc, ca, cb, cc
# aaa, aab, aac, aba, abb, abc, ...

def bruteforce(n):
    if n == 0:
        yield ""
    else:
        prev = bruteforce(n-1)
        ans = []
        for p in prev:
            for c in ascii_letters:
                yield p + c

g = bruteforce(10)
for i in g:
    print(i)

# Try implementing combination for homework

# Imagine theres a rope around the Earth's diameter (tight).
# We add 6m to the rope such that it becomes slack.
# Part 1) How far above the equator (diameter) does the rope go?

# Hard* Part 2) Imagine that I now pull the rope at a point away
# from the equator. It will sort of look like the rope is hugging
# on one half and shaping into a triangle on the other side.
# How high is the tip of the triangle from the Earth's surface?

# Hint: Use what you know about arcs on a circle, circumference, tangent lines. At some point, you may have some equation that you don't know how to solve algebraically, but can solve numerically (using bisection).
